from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

class TestUnit(TestCase):
    def test_adakah_halamannya(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_text(self):
        response = Client().get('/')
        html_return = response.content.decode('utf8')
        self.assertIn("Welcome to Story PPW", html_return)
        self.assertIn("Identitas", html_return)
        self.assertIn("Hobi", html_return)
        self.assertIn("Sekolah", html_return)
        self.assertIn("Aktivitas", html_return)
        self.assertIn("Organisasi", html_return)





    



